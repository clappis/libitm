#!/bin/bash

benchs=('bayes' 'genome' 'intruder' 'kmeans' 'labyrinth' 'ssca2' 'vacation' 'yada') 
threads=(1 2 4 8 16 32)  

STAMP_DIR=/home/alan.clappis/Desenvolvimento/others/concorrentes/transactional/stamp_libitm/gcctm
MAX_ROUNDS=30

PERF="perf stat -e branch-instructions -e branch-misses -e bus-cycles -e cache-misses -e cache-references -e cpu-cycles -e instructions -e ref-cycles -e stalled-cycles-frontend -e alignment-faults -e context-switches -e cpu-clock -e cpu-migrations -e dummy -e emulation-faults -e major-faults -e minor-faults -e page-faults -e task-clock"

for (( b=0; b<${#benchs[@]}; b++ )); do

	cd $STAMP_DIR
	bench=${benchs[${b}]}
	cd $bench
	
	for (( t=0; t<${#threads[@]}; t++ )); do

		thread=${threads[${t}]}

		for (( r=10; r<MAX_ROUNDS; r++ )); do

			if [[ $bench == "bayes" ]]; then
				$PERF -o bayes_perf.thread_$thread.run.$r ./bayes -v32 -r4096 -n10 -p40 -i2 -e8 -s1 -t$thread > bayes_result_thread_$thread.run.$r
			elif [[ $bench == "genome" ]]; then
				$PERF -o genome.perf.thread.$thread.run.$r ./genome -g16384 -s64 -n16777216 -t$thread > genome.result.thread.$thread.run.$r
			elif [[ $bench == "intruder" ]]; then	
				$PERF -o intruder.perf.thread.$thread.run.$r ./intruder -a10 -l128 -n262144 -s1 -t$thread > intruder.result.thread.$thread.run.$r
			elif [[ $bench == "kmeans" ]]; then
				$PERF -o kmeans_low.perf.thread.$thread.run.$r ./kmeans -m40 -n40 -t0.00001 -i ../../inputs/kmeans/random-n65536-d32-c16.txt -t$thread > kmeans_low.result.thread.$thread.run.$r
				$PERF -o kmeans_high.perf.thread.$thread.run.$r ./kmeans -m15 -n15 -t0.00001 -i ../../inputs/kmeans/random-n65536-d32-c16.txt -t$thread > kmeans_high.result.thread.$thread.run.$r
			elif [[ $bench == "labyrinth" ]]; then
				$PERF -o labyrinth.perf.thread.$thread.run.$r ./labyrinth -i ../../inputs/labyrinth/random-x512-y512-z7-n512.txt -t$thread > labyrinth.result.thread.$thread.run.$r
			elif [[ $bench == "ssca2" ]]; then
				$PERF -o ssca2.perf.thread.$thread.run.$r ./ssca2 -s20 -i1.0 -u1.0 -l3 -p3 -t$thread > ssca2.result.thread.$thread.run.$r
			elif [[ $bench == "vacation" ]]; then
				$PERF -o vacation_low.perf.thread.$thread.run.$r  ./vacation -n2 -q90 -u98 -r1048576 -L -T4194304 -c$thread > vacation_low.result.thread.$thread.run.$r
				$PERF -o vacation_high.perf.thread.$thread.run.$r ./vacation -n4 -q60 -u90 -r1048576 -T4194304 -c$thread > vacation_high.result.thread.$thread.run.$r
			elif [[ $bench == "yada" ]]; then
				$PERF -o yada.perf.thread.$thread.run.$r ./yada -a15 -i ../../inputs/yada/ttimeu1000000.2 -t$thread > yada.result.thread.$thread.run.$r
			fi
		done

	done
	cd ..
done
