#ifndef LIBITM_WRAPPER
#define LIBITM_WRAPPER

void notify_abort();
void notify_trycommit();
void notify_commit();
void show_transaction_results();

unsigned static long count_abort;
unsigned static long count_commit;
unsigned static long count_trycommit;


#endif

