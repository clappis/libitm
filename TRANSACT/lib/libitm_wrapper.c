#include "libitm_wrapper.h"
#include <stdio.h>

void notify_abort(){
	count_abort++;
}

void notify_commit(){
	count_commit++;
}

void notify_trycommit(){
	count_trycommit++;
}

void show_transaction_results(){
	printf("\n -- TRANSACTIONS RESULTS --\n");
	printf("Count TryCommit: %lu", count_trycommit);
	printf("\nCount Commit: %lu", count_commit);
	printf("\nCount Abort: %lu", count_abort);
	printf("\n ----------------------\n");
}
